import time
import pandas as pd
import psycopg2 as pg
import warnings


with warnings.catch_warnings(record=True):
    warnings.simplefilter("always")
    engine = pg.connect(
        "dbname='postgres' user='postgres' host='127.0.0.1' port='5432' password='postgres'"
    )

    start = time.time() ## точка отсчета времени
    df = pd.read_sql("select * from actor limit 3 ", con=engine)
    end = time.time() - start ## собственно время работы программы
    print(end) ## вывод времени

print(df)
