import pandas as pd


class ReadDoc:
    def getDoc():
        airports = pd.read_csv("data/airports.csv")
        airport_freq = pd.read_csv("data/airport-frequencies.csv")
        runways = pd.read_csv("data/runways.csv")

        return airports, airport_freq, runways
