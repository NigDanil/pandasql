# Команды

## Установка venv

```bash
python -m venv venv
```

### Включение venv - для Windows

```bash
venv\Scripts\activate.bat
venv\Scripts\deactivate.bat
```
### Включение venv - для Linux и MacOS

```bash
source venv/bin/activate
```

## Установка requirements

```bash
pip install -r requirements.txt
```