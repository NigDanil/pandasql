# Работа с данными по-новому: Pandas вместо SQL

Раньше SQL как инструмента было достаточно для исследовательского анализа: быстрого поиска данных и предварительного отчёта по ним.

Сейчас данные бывают разных форм и не всегда под ними подразумевают «реляционные базы данных». Это могут быть CSV-файлы, простой текст, [Parquet](https://habr.com/ru/companies/wrike/articles/279797/), [HDF5](https://ru.wikipedia.org/wiki/Hierarchical_Data_Format) и многое другое. Здесь вам и поможет библиотека [Pandas](https://ru.wikipedia.org/wiki/Pandas).
Возможность использования в связке с ORM [SQLAlchemy](https://ru.wikipedia.org/wiki/SQLAlchemy):

- Использование ORM не является обязательным
- Устоявшаяся архитектура
- Возможность использовать SQL, написанный вручную
- Поддержка транзакций
- Создание запросов с использованием функций и выражений Python
- Модульность и расширяемость
- Дополнительная возможность раздельного определения объектного отображения и классов
- Поддержка составных индексов
- Поддержка отношений между классами, в том числе «один-ко-многим» и «многие-ко-многим»
- Поддержка ссылающихся на себя объектов
- Предварительная и последующая обработка данных (параметров запроса, результата)

Ссылки:
[Pandas](https://pandas.pydata.org/)
[Оригинал статьи](https://tproger.ru/translations/rewrite-sql-queries-in-pandas/)
Clone with HTTPS:
https://gitlab.com/NigDanil/pandasql.git

- Для работы с примерами необходим :smirk: **Python** 3.09 и более.
- Прописать в **Переменные среды** путь к **Python**. 
- Установка окружения (**venv**).
- Запуск окружения (**venv**).
- Установка необходимых пакетов из файла `requirements.txt`.
- :no_entry_sign: Установливать **Conda** не нужно!
- Раскомментировать нужный пример.
- Запустить **cmd** и запустить скрипт main.py
```bash
(venv)main.py
```
- [Шпора Pandas](Pandas_Cheat_Sheet.pdf)

## Что такое Pandas?
Pandas — это библиотека Python для обработки и анализа структурированных данных, её название происходит от «panel data» («панельные данные»). Панельными данными называют информацию, полученную в результате исследований и структурированную в виде таблиц. Для работы с такими массивами данных и создан Pandas.

SQL — декларативный язык. Он позволяет объявлять всё таким образом, что запрос похож на обычное предложение в английском языке. Синтаксис Pandas сильно отличается от SQL. Здесь вы применяете операции к набору данных и объединяете их в цепочку для преобразования и изменения.

## Разбор SQL-запроса
SQL-запрос состоит из нескольких ключевых слов. Между этими словами добавляются характеристики данных, которые вы хотите видеть. Пример каркаса запросов без конкретики:

```
SELECT… FROM… WHERE…
GROUP BY… HAVING…
ORDER BY…
LIMIT… OFFSET…
```
Есть и другие выражения, но эти самые основные. Чтобы перевести выражения в Pandas, нужно сначала загрузить данные:

```py
import pandas as pd

airports = pd.read_csv('data/airports.csv')
airport_freq = pd.read_csv('data/airport-frequencies.csv')
runways = pd.read_csv('data/runways.csv')
```

### SELECT, WHERE, DISTINCT, LIMIT
Ниже представлено несколько вариантов выражений с оператором `SELECT`. Ненужные результаты отсекаются с помощью `LIMIT` и отфильтровываются с помощью `WHERE`. Для удаления дублированных результатов используется `DISTINCT`.

<details>
<summary><b>Выражение на SQL</b></summary>

```sql
select * from airports
```
```sql                 
select * from airports limit 3
```
```sql              
select id from airports 
where ident = 'KLAX'
```
```sql 
select distinct type from airport
```
</details>
        
<details>
<summary><b>Выражение на Python</b></summary>

```py
airports
```
```py
airports.head(3)
```
```py
airports[airports.ident == 'KLAX'].id
```
```py
airports.type.unique()
```
</details>

### SELECT со множественным условием
Несколько условий выбора объединяются с помощью операнда &. Если нужно только подмножество некоторых столбцов из таблицы, это подмножество применяется в другой паре квадратных скобок.

<details>
<summary><b>Выражение на SQL</b></summary>

```sql
select * from airports 
where iso_region = 'US-CA' and type = 'seaplane_base'
```
```sql
select ident, name, municipality from airports 
where iso_region = 'US-CA' and type = 'large_airport'  
```
</details>

<details>

<summary><b>Выражение на Python</b></summary>

```py
airports[(airports.iso_region == 'US-CA') & 
(airports.type == 'seaplane_base)]
```
```py
airports[(airports.iso_region == 'US-CA') & 
(airports.type == 'large_airport')][['ident', 'name', 'municipality']]
```
</details>

### ORDER BY
По умолчанию Pandas сортирует данные по возрастанию. Для обратной сортировки используйте выражение `ascending=False`.

<details>
<summary><b>Выражение на SQL</b></summary>

```sql
select * from airport_freq 
where airport_ident = 'KLAX' 
order by type
```
```sql
select * from airport_freq 
where airport_ident = 'KLAX' 
order by type desc
```
</details>

<details>
<summary><b>Выражение на Python</b></summary>

```py
airport_freq[airport_freq.airport_ident == 'KLAX']
.sort_values('type')
```
```py
airport_freq[airport_freq.airport_ident == 'KLAX']
.sort_values('type', ascending = False)
```
</details>

### IN и NOT IN
Чтобы фильтровать не одно значение, а целые списки, существует условие `IN`. В Pandas оператор `.isin()` работает точно так же. Чтобы отменить любое условие, используйте `~` (**тильда**).

<details>
<summary><b>Выражение на SQL</b></summary>

```sql
select * from airports 
where type in ('heliport', 'ballonport')
```
```sql
select * from airports 
where type not in ('heliport', 'ballonport')
```
</details>

<details>
<summary><b>Выражение на Python</b></summary>

```py
airports[airports.type.isin(['heliport', 'ballonport'])]
```
```py
airports[~airports.type.isin(['heliport', 'ballonport'])]
```
</details>

### GROUP BY, COUNT, ORDER BY
Группировка осуществляется с помощью оператора `.groupby()`. Есть небольшая разница между семантикой `COUNT` в SQL и Pandas. В Pandas `.count()` вернёт значения `non-null/NaN`. Для получения результата как в SQL, используйте .`size()`.

<details>
<summary><b>Выражение на SQL</b></summary>

```sql
select iso_country, type, count(*) from airports 
group by iso_country, 
type order by iso_country, type
```
```sql
select iso_country, type, count(*) from airports 
group by iso_country, 
type order by iso_country, 
count(*) desc 
```
</details>

<details>
<summary><b>Выражение на Python</b></summary>

```py
airports.groupby(['iso_country', 'type'])
.size()
```
```py
airports.groupby(['iso_country', 'type'])
.size().to_frame('size')
.reset_index()
.sort_values(['iso_country', 'size'], ascending=[True, False])
```
</details>

Ниже приведена группировка по нескольким полям. По умолчанию Pandas сортирует по одному и тому же списку полей, поэтому в первом примере нет необходимости в `.sort_values()`. Если нужно использовать разные поля для сортировки или `DESC` вместо `ASC`, как во втором примере, выборку необходимо задавать явно:


<details>
<summary><b>Выражение на SQL</b></summary>

```sql
select iso_country, type, count(*) from airports
group by iso_country, 
type order by iso_country type
```
```sql
select iso_country, type, count(*) from airports
group by iso_country, 
type order by iso_country count(*) desc
```
</details>

<details>
<summary><b>Выражение на Python</b></summary>

```py
airports.groupby(['iso_country', 'type'])
.size()
```
```py
airports.groupby(['iso_country', 'type'])
.size()
.to_frame("size")
.reset_index()
.sort_values(['iso_country', 'size'], ascending=[True, False])
```
</details>

Использование `.to_frame()` и `.reset_index()` обуславливается сортировкой по конкретному полю (`size`). Это поле должно быть частью типа **DataFrame**. После группировки в Pandas в результате получается другой тип, называемый `GroupByObject`. Поэтому нужно преобразовать его обратно в **DataFrame**. С помощью `.reset_index()` перезапускается нумерация строк для фрейма данных.

### HAVING
В SQL можно дополнительно фильтровать сгруппированные данные, используя условие `HAVING`. В Pandas можно использовать `.filter()` и предоставить функцию Python (или лямбда-выражение), которая будет возвращать `True`, если группа данных должна быть включена в результат.

<details>
<summary><b>Выражение на SQL</b></summary>

```sql
select type, count(*) from airports 
where iso_country = 'US' group by type having count(*) > 1000 
order by count(*) desc
```
</details>

<details>
<summary><b>Выражение на Python</b></summary>

```py
airports[airports.iso_country == 'US']
.groupby('type')
.filter(lambda g: len(g) > 1000)
.groupby('type')
.size()
.sort_values(ascending = False])
```
</details>

### Агрегатные функции: MIN, MAX, MEAN

Учитывая фрейм данных выше (данные взлётно-посадочной полосы), рассчитаем минимальную, максимальную и среднюю длину ВПП.

<details>
<summary><b>Выражение на SQL</b></summary>

```sql
select max(length_ft), min(length_ft), mean(length_ft), median(length_ft) from runways
```
</details>

<details>
<summary><b>Выражение на Python</b></summary>

```py
runways.agg({'length_ft': ['min', 'max', 'mean', 'median']})
```
</details>

Заметьте, что с SQL-запросом данные представляют собой столбцы. Но в Pandas данные представлены строками.

Фрейм данных можно легко транспонировать с помощью .T, чтобы получить столбцы.

### JOIN
Используйте `.merge()`, чтобы присоединить фреймы данных в Pandas. Необходимо указать, к каким столбцам нужно присоединиться (`left_on` и `right_on`), а также тип соединения: `inner` (по умолчанию), `left` (соответствует `LEFT OUTER` в SQL), right (`RIGHT OUTER` в SQL) или outer (`FULL OUTER` в SQL).

<details>
<summary><b>Выражение на SQL</b></summary>

```sql
select airport_ident, type, description, frequency_mhz from airport_freq 
join airports on airport_freq.airport_ref = airports.id 
where airports.ident = 'KLAX'
```
</details>

<details>
<summary><b>Выражение на Python</b></summary>

```py
airport_freq.merge(
    airports[airports.ident == "KLAX"][["id"]],
    left_on="airport_ref",
    right_on="id",
    how="inner",
)[["airport_ident", "type", "description", "frequency_mhz"]]
```
</details>

### UNION ALL и UNION
`pd.concat() `— эквивалент UNION ALL в SQL.

<details>
<summary><b>Выражение на SQL</b></summary>

```sql
select name, municipality from airports 
where ident = 'KLAX' union all select name, municipality from airports 
where ident = 'KLGB'
```
</details>

<details>
<summary><b>Выражение на Python</b></summary>

```py
pd.concat(
    [
        airports[airports.ident == "KLAX"][["name", "municipality"]],
        airports[airports.ident == "KLGB"][["name", "municipality"]],
    ]
)
```
</details>

Эквивалентом `UNION` (дедупликация) является `.drop_duplicates()`.

### INSERT
Пока осуществлялась только выборка, но в процессе предварительного анализа данные можно изменить. В Pandas для добавления нужных данных нет эквивалента `INSERT` в SQL. Вместо этого следует создать новый фрейм данных, содержащий новые записи, а затем объединить два фрейма.

<details>
<summary><b>Выражение на SQL</b></summary>

```sql
create table heroes (id integer, name text);
```
```sql
insert into heroes values (1, 'Harry Potter');
```
```sql
insert into heroes values (2, 'Ron Weasley');
```
```sql
insert into heroes values (2, 'Hermione Weasley');
```
</details>

<details>
<summary><b>Выражение на Python</b></summary>

```py
df1 = pd.DataFrame({'id': [1, 2], 'name': ['Harry Potter', 'Ron Weasley']})
df2 = pd.DataFrame({'id': [3], 'name': ['Hermione Weasley']})
pd.concat([df1, df2]).reset_index(drop=True)
```
</details>

### UPDATE
Предположим, теперь нужно исправить некоторые неверные данные в исходном фрейме.

<details>
<summary><b>Выражение на SQL</b></summary>

```sql
update airports set home_link = 'http://www.lawa.org/welcomelax.aspx' where ident == 'KLAX'
```
</details>

<details>
<summary><b>Выражение на Python</b></summary>

```py
print(airports.loc[airports['ident'] == 'KLAX', 'home_link'])
temp_var = airports.loc[airports['ident'] == 'KLAX', 'home_link'] = 'http://www.lawa.org/welcomelax.aspx'
print(temp_var)
```
</details>

### DELETE
Самый простой и удобный способ удалить данные из фрейма в Pandas — это разбить фрейм на строки. Затем получить индексы строк и использовать их в методе `.drop()`.

<details>
<summary><b>Выражение на Python</b></summary>

```py
df = pd.DataFrame(np.arange(12)
.reshape(3, 4), columns=["A", "B", "C", "D"])
df.drop(["B", "C"], axis=1)

```
</details>

### Неизменность
По умолчанию большинство операторов в Pandas возвращают новый объект. Некоторые операторы принимают параметр `inplace=True`, что позволяет работать с исходным фреймом данных вместо нового. Например, вот так можно сбросить индекс на месте:

```py
df.reset_index(drop=True, inplace=True)
```

Однако оператор `.loc` (выше в примере с `UPDATE`) просто находит индексы записей для их обновления, и значения меняются на месте. Также, если все значения в столбце обновлены (d`f['url'] = 'http://google.com'`) или добавлен новый столбец (`df['total_cost'] = df['price'] * df['quantity']`), эти данные изменятся на месте.

Pandas — это больше, чем просто механизм запросов. С данными можно делать и другие преобразования.

### Экспорт во множество форматов
```py
df.to_csv(...)  # в csv-файл
df.to_hdf(...)  # в HDF5-файл
df.to_pickle(...)  # в сериализованный объект
df.to_sql(...)  # в базу данных SQL
df.to_excel(...)  # в файл Excel
df.to_json(...)  # в строку JSON
df.to_html(...)  # отображение в качестве HTML-таблицы
df.to_feather(...)  # в двоичный feather-формат
df.to_latex(...)  # в табличную среду
df.to_stata(...)  # в бинарные файлы данных Stata
df.to_msgpack(...)  # msgpack-объект (сериализация)
df.to_gbq(...)  # в BigQuery-таблицу (Google)
df.to_string(...)  # в консольный вывод
df.to_clipboard(...) # в буфер обмена, который может быть вставлен в Excel
```
### Составление графиков

![img](../img/Figure_1.png)

```py
import pandas as pd
import matplotlib.pyplot as plt


dataset_url = (
    "https://raw.githubusercontent.com/m-mehdi/pandas_tutorials/main/weekly_stocks.csv"
)
df = pd.read_csv(dataset_url, parse_dates=["Date"], index_col="Date")
pd.set_option("display.max.columns", None)
print(df.head())
df.plot()
plt.show()
```
