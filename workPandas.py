import workDocuments as w_doc
import pandas as pd
import numpy as np


class Crawler:
    global lax_freq

    def loadData():
        airports = w_doc.ReadDoc.getDoc()[0]
        airport_freq = w_doc.ReadDoc.getDoc()[1]
        runways = w_doc.ReadDoc.getDoc()[2]

        # SELECT, WHERE, DISTINCT, LIMIT
        # -----------------------------------------------------------------
        # print(airports)
        # print(airports.head(3))
        # print(airports[airports.ident == 'KLAX'].id)
        # print(airports.type.unique())
        # -----------------------------------------------------------------
        # SELECT со множественным условием
        # -----------------------------------------------------------------
        # print(
        #     airports[
        #         (airports.iso_region == "US-CA") & (airports.type == "seaplane_base")
        #     ]
        # )

        # print(
        #     airports[
        #         (airports.iso_region == "US-CA") & (airports.type == "large_airport")
        #     ][["ident", "name", "municipality"]]
        # )
        # -----------------------------------------------------------------
        # ORDER BY
        # print(airport_freq[airport_freq.airport_ident == "KLAX"].sort_values("type"))
        # print(
        #     airport_freq[airport_freq.airport_ident == "KLAX"].sort_values(
        #         "type", ascending=False
        #     )
        # )
        # -----------------------------------------------------------------
        # IN и NOT IN
        # -----------------------------------------------------------------
        # print(airports[airports.type.isin(['heliport', 'ballonport'])])
        # print(airports[~airports.type.isin(['heliport', 'ballonport'])])
        # -----------------------------------------------------------------
        # GROUP BY, COUNT, ORDER BY
        # -----------------------------------------------------------------
        # print(airports.groupby(["iso_country", "type"]).size())

        # print(airports.groupby(["iso_country", "type"]).size().to_frame(
        #     "size"
        # ).reset_index().sort_values(["iso_country", "size"], ascending=[True, False]))
        # -----------------------------------------------------------------
        # print(airports.groupby(["iso_country", "type"]).size())
        # print(
        #     airports.groupby(["iso_country", "type"])
        #     .size()
        #     .to_frame("size")
        #     .reset_index()
        #     .sort_values(["iso_country", "size"], ascending=[True, False])
        # )
        # -----------------------------------------------------------------
        # HAVING
        # -----------------------------------------------------------------
        # print(
        #     airports[airports.iso_country == "US"]
        #     .groupby("type")
        #     .filter(lambda g: len(g) > 1000)
        #     .groupby("type")
        #     .size()
        #     .sort_values(ascending=False)
        # )
        # -----------------------------------------------------------------
        # Агрегатные функции: MIN, MAX, MEAN
        # -----------------------------------------------------------------
        # print(runways.agg({"length_ft": ["min", "max", "mean", "median"]}))
        # -----------------------------------------------------------------
        # JOIN
        # -----------------------------------------------------------------
        # print(
        #     airport_freq.merge(
        #         airports[airports.ident == "KLAX"][["id"]],
        #         left_on="airport_ref",
        #         right_on="id",
        #         how="inner",
        #     )[["airport_ident", "type", "description", "frequency_mhz"]]
        # )
        # -----------------------------------------------------------------
        # UNION ALL и UNION
        # -----------------------------------------------------------------
        # print(
        #     pd.concat(
        #         [
        #             airports[airports.ident == "KLAX"][["name", "municipality"]],
        #             airports[airports.ident == "KLGB"][["name", "municipality"]],
        #         ]
        #     )
        # )
        # -----------------------------------------------------------------
        # INSERT
        # -----------------------------------------------------------------
        # df1 = pd.DataFrame({'id': [1, 2], 'name': ['Harry Potter', 'Ron Weasley']})
        # df2 = pd.DataFrame({'id': [3], 'name': ['Hermione Weasley']})
        # print(pd.concat([df1, df2]).reset_index(drop=True))
        # -----------------------------------------------------------------
        # UPDATE
        # -----------------------------------------------------------------
        # print(airports.loc[airports['ident'] == 'KLAX', 'home_link'])
        # temp_var = airports.loc[airports['ident'] == 'KLAX', 'home_link'] = 'http://www.lawa.org/welcomelax.aspx'
        # print(temp_var)
        # -----------------------------------------------------------------
        # DELETE
        # -----------------------------------------------------------------
        # df = pd.DataFrame(np.arange(12).reshape(3, 4), columns=["A", "B", "C", "D"])
        # print(df)
        # print(df.drop(["B", "C"], axis=1))
