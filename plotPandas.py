import pandas as pd
import matplotlib.pyplot as plt


dataset_url = (
    "https://raw.githubusercontent.com/m-mehdi/pandas_tutorials/main/weekly_stocks.csv"
)
df = pd.read_csv(dataset_url, parse_dates=["Date"], index_col="Date")
pd.set_option("display.max.columns", None)
print(df.head())
df.plot()
plt.show()
